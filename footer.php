<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Inspine Institue_Theme
 */

?>

    </div>
    <!-- #content -->

    <footer id="main-footer" class="site-footer">
        <!--FOOTER BG COLOR -->
        <div class="container main-footer-area">
            <div class="row">
                <div class="col-sm-12">
                    <div class="top-footer">
                        
                        <h1 class="footer_social_share_title"><?php the_field('social_share_section_title', option);?></h1>
                        <ul class="social-media-icon">

                        <?php

                            // check if the repeater field has rows of data
                            if( have_rows('social_repeater', option) ):
                                while ( have_rows('social_repeater', option) ) : the_row(); ?>
                                <li>
                                <a href="<?php the_sub_field('social_url')?>">
                                    <i class="<?php the_sub_field('icon_for_social')?>"></i>
                                </a>
                                </li>
                               <?php endwhile;
                            endif;
                            ?>

                        </ul>
                    </div>
                    
                </div>


            </div>


            <div class="row ">
                <div class="col-sm-12 footer_nav">
                    <div class="footer_nav__list">
                    <?php
                    wp_nav_menu(
                    array(
                    'theme_location' => 'footer_menu',
                    'container'       => false,
                    'menu_id'      => 'footer_menu__list',
                    'menu_class'         => 'footer_menu__list',
                    ));
                    ?>
                    </div>
                </div>
            </div>



        </div>
        <!-- close .container -->

        <div class="footer-copyinfo">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <?php if('copy_right_info') : ?>
                        <?php the_field('copy_right_info', 'option'); ?>
                        <?php endif ?>
                    </div>

                </div>
            </div>
        </div>
    </footer>
    <!-- close #colophon -->




    </div>
    <!-- #page -->

    <?php wp_footer(); ?>

    </body>

    </html>