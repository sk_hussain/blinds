<?php

add_shortcode('testimonial_slider', 'shortcode_testimonial_slider');

function shortcode_testimonial_slider($atts, $content = null) {
    extract( shortcode_atts( array(
	// 	  'variable_1'	=> '',
    //   'variable_2'	=> '',
    //   'variable_3'	=> '',
      
	), $atts ) );
    ob_start();
?>


<div class="slider_testimonials owl-carousel owl-theme">
<?php
// check if the repeater field has rows of data
if( have_rows('testimonials_repeater',option) ):

 	// loop through the rows of data
    while ( have_rows('testimonials_repeater',option) ) : the_row(); ?>

    <div class="item testimonial_item">
        <h3><?php the_sub_field('content_testimonial', option); ?></h3>
        <h5><?php the_sub_field('author_testimonial', option); ?></h5>
    </div>


<?php    endwhile;
endif;

?>

</div>

<script>
    jQuery(document).ready(function ($) {
    $('.slider_testimonials').owlCarousel({
    loop:true,
    autoplay:true,
    autoplayHoverPause:true,
    dots:true,
    items:1,
    
})

});

</script>

<?php

    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [testimonial_slider][/testimonial_slider]  **/